import configparser
import json
import logging
import os
import shutil
import sys

import jinja2

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

MAIN_CONFIG_FILE = 'config.ini'

config = configparser.ConfigParser()
config.read(MAIN_CONFIG_FILE)

class VarsNotFoundException(Exception):
    pass


def get_output_filename_from_src_filename(filename: str) -> str:
    # fixme two params from global vars
    outname = filename.replace('.' + input_file_extension,
                               '.' + output_file_extension)
    return outname


def get_jvars(file_path: str) -> dict[str]:
    # todo replace jvars in a unique row, to be multirow for convenience (something like pelican)
    jvars = {}
    # added filename in jvars
    # todo maybe add create date and update date?
    # todo page creation and last updated if available else from file metadata
    jvars['filename'] = get_output_filename_from_src_filename(os.path.basename(file_path))
    with open(file_path, 'r') as src:
        src_content = src.read()
        try:
            jvars_str = src_content.splitlines()[0]
            if jvars_str.startswith('<!-- {') and jvars_str.endswith('} -->'):
                logging.debug("found src vars!")
                jvars_str = jvars_str.removeprefix('<!--').removesuffix('-->').strip()
                logging.debug(jvars_str)
                jvars = jvars | json.loads(jvars_str)
            else:
                logging.warning("src vars not found in file %s!", file_path)
                if not continue_on_missing_var:
                    raise VarsNotFoundException(f'No vars found on page {file_path}')
        except IndexError:
            logging.warning('File %s is empty!', file_path)
    logging.debug(jvars)
    return jvars


def gen_index(src_dir: str, out_folder: str, input_file_extension: str, template_file_extension: str,
              template_vars: dict[str], template_obj, output_file_extension: str, template_env, template_paths: list[str]) -> int:
    files_to_index = []
    # first cycle for findind index src file, if present we exit immediately
    files_counter = 0
    for entry in os.scandir(src_dir):
        files_counter += 1
        # fixme exclude files to filter (like disabled) from files counter
        if entry.name == 'index.src.html':  # todo save in config file
            return 0
        elif entry.name.endswith(template_file_extension):
            logging.debug('Template path %s', src_dir)
            logging.debug('Found new template to override in %s!', src_dir)
            logging.debug('Template path %s', entry.path)
            template_paths.append(src_dir)
            template_loader_2 = jinja2.FileSystemLoader(template_paths)
            template_env_2 = jinja2.Environment(loader=template_loader_2)
            template_obj = template_env_2.get_template(entry.name)

    if files_counter == 0:
        # if the directory is empty no index file is generated
        # fixme maybe treat this case better
        return 0
    # jvars not needed for index file
    # second cycle in case the file is missing
    for entry in os.scandir(src_dir):
        if entry.is_file() and entry.name.endswith(input_file_extension):
            jvars = get_jvars(entry.path)
            # todo add options to use url_rewrite to map 2022-09-22-1 to 2022-09-22-1.html
            files_to_index.append(jvars)

    # fixme manage template intheritance in subfolders
    # cannot find template if is only define in parent folder

    out_content = template_obj.render(template_vars, content=files_to_index)
    outname = 'index.html'
    with open(os.path.join(out_folder, outname), 'w') as fw:
        fw.write(out_content)
        logging.info("Wrote file %s", outname)
        return 0


# FIXME maybe use DirEntry for root and out folders an template path
# TODO *handle template inheritance!*
def gen_out_tree(root: str, depth: int, out_folder: str, template_obj: jinja2.Template,
                 max_folder_depth: int, template_extension: str,
                 input_file_extension: str, output_file_extension: str, copy_file_extensions: list[str],
                 template_vars: dict[str], continue_on_missing_var: bool, index_template: str, template_env,
                 template_paths: list[str]) -> int:
    if depth > max_folder_depth:
        logging.warning("Max depth raised %s", max_folder_depth)
        return 1
    logging.debug("in: %s, out: %s, depth: %s.", root, out_folder, depth)
    # first we generate the current out folder
    # FIXME don't make folder if empty?
    try:
        # os.makedirs(out_folder)
        os.mkdir(out_folder)  # we don't need the recursive one
    except FileExistsError as fe:
        logging.info("Directory %s exists, skipping mkdir.", out_folder)
    folders = []
    src_files = []
    files_to_copy = []
    # second we separate files from directories so we can parse files first
    for entry in os.scandir(root):
        if entry.is_dir():
            folders.append(entry)
        elif entry.is_file():
            if entry.name.endswith(template_extension):
                template_path = os.path.dirname(entry.path)
                logging.info("found new template in path %s", template_path)
                logging.info('Template name %s', entry.name)
                logging.info("Initializing new template")
                template_paths.append(template_path)
                template_loader_2 = jinja2.FileSystemLoader(template_paths)
                new_template_env = jinja2.Environment(loader=template_loader_2)
                # replace old template
                template_obj = new_template_env.get_template(entry.name)
                logging.debug("New template object %s", template_obj)
            elif entry.name.endswith(input_file_extension):
                logging.debug('Found source file %s', entry.name)
                src_files.append(entry)
            elif entry.name.endswith(tuple(copy_file_extensions)):
                logging.debug('Found file to copy as-is: %s', entry.name)
                files_to_copy.append(entry)
            else:
                logging.debug('File %s ignored', entry.name)
    # forth we parse all "source" html file
    for f in src_files:
        logging.debug('Input file %s', f.path)
        jvars = get_jvars(f.path)
        with open(f.path, 'r') as src:
            #    # we explicitly don't catch exception if the file is not present
            src_content = src.read()
            # FIXME maybe remove the first line from content (if src_var is found) without using splitlines()
            out_content = template_obj.render(template_vars | jvars, content=src_content)
            outname = get_output_filename_from_src_filename(f.name)
            with open(os.path.join(out_folder, outname), 'w') as fw:
                fw.write(out_content)
                logging.info("Wrote file %s", outname)
    for f in files_to_copy:
        shutil.copy2(f, out_folder)  # copy2 tries to preserve metadata

    # generate index
    logging.debug('Begin gen_index()')
    template_obj_index = template_env.get_template(index_template)
    # fixme index file extension hardcoded
    gen_index(root, out_folder, input_file_extension, '.index.html.jinja', template_vars, template_obj_index,
              output_file_extension, template_env, template_paths)
    logging.debug('end gen_index()')

    # fifth we recurse on all the directories
    logging.debug('Folders found: %s', folders)
    for d in folders:
        head, tail = os.path.split(d)  # to extract the current dir name
        # recurse
        # we could just use d as DirEntry, but we convert to path as for the root that is string
        gen_out_tree(d.path, depth + 1, os.path.join(out_folder, tail), template_obj,
                     max_folder_depth, template_extension, input_file_extension, output_file_extension, copy_file_extensions,
                     template_vars, continue_on_missing_var, index_template, template_env, template_paths)
    return 0


if __name__ == "__main__":

    top = config['main']['input_folder']
    out = config['main']['output_folder']
    main_template_name = config['template']['main_template_name']
    template_extension = config['template']['page_template_extension']
    max_folder_depth = config['main'].getint('max_folder_depth', 10)
    input_file_extension = config['template']['input_file_extension']
    output_file_extension = config['template']['output_file_extension']
    copy_file_extensions_str = config['template']['copy_file_extensions']
    logging.info(copy_file_extensions_str)
    copy_file_extensions = copy_file_extensions_str.split(',')
    new_exts = []
    for extension in copy_file_extensions:
        new_ext = extension.strip(' .')
        new_ext = '.' + new_ext
        new_exts.append(new_ext)
    global_vars = dict(config.items('vars'))
    continue_on_missing_var = config['template'].getboolean('continue_on_missing_var')
    index_template = config['template']['list_template_name']

    logging.info(new_exts)

    logging.info("Root source folder: %s", top)
    logging.info("Root target folder: %s", out)

    templateLoader = jinja2.FileSystemLoader([top])
    templateEnv = jinja2.Environment(loader=templateLoader)
    template_obj = templateEnv.get_template(main_template_name)

    exit_code = gen_out_tree(top, 0, out, template_obj, max_folder_depth, template_extension,
                             input_file_extension, output_file_extension, new_exts, global_vars,
                             continue_on_missing_var, index_template, templateEnv, [top])

    # todo look at babel for i18n https://babel.pocoo.org/en/latest/

    sys.exit(exit_code)
